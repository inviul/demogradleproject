package com.inviul.test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.Logger;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Map;
import java.util.Set;

public class RestMain {

    public static Logger logger = BaseTest.getLogData(RestMain.class.getName());

    public static ExtentReports reports;
    public static ExtentTest test;

    public static void main(String... args){
    System.out.println("Job Name: "+System.getenv("val"));
       System.out.println("Job Name: "+System.getenv("JOB_NAME"));
        String reportPath = new File("").getAbsoluteFile().toString().trim()+"/TestReport/";
        reports = new ExtentReports(reportPath+RestMain.class.getSimpleName()+"_Report.html",false);
        test= BaseTest.generateTest(reports, RestMain.class.getSimpleName(), "This test is to validate the GitHub Team API");
        RESTUtility.setBaseURI("https://api.github.com/");
        RequestSpecification request= RESTUtility.requestSpecification("97a4fa00039601898dfef782054480d3a11f04a4");
        Response response = RESTUtility.getDefaultResponse(request);
        int responseCodeBaseUrl = RESTUtility.getStatusCode(response);
        logger.info("Running Time: "+System.currentTimeMillis());
        RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();

        Map<String, String> systemProperties = runtimeBean.getSystemProperties();
        Set<String> keys = systemProperties.keySet();
        for (String key : keys) {
            String value = systemProperties.get(key);
            logger.info("[%s] = %s.\n," +key+","+value);
            BaseTest.reportPass(test, RestMain.class.getName(), "[%s] = %s.\n," +key+","+value);
        }
        logger.info("Response Code: "+responseCodeBaseUrl);
        BaseTest.reportPass(test, RestMain.class.getName(), String.valueOf(responseCodeBaseUrl));
        BaseTest.tearDown(test,reports);
    }
}
