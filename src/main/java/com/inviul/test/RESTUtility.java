package com.inviul.test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class RESTUtility {
    public static String path; //Rest request path

    public static void setBaseURI (String baseURI){
        RestAssured.baseURI = baseURI;
    }

    public static void resetBaseURI(String baseUrl){
        RestAssured.baseURI = null;
    }

    public static RequestSpecification requestSpecification(String accessToken){
        return RestAssured.given().auth().oauth2(accessToken);
    }

    public static Response getDefaultResponse(RequestSpecification httpReq){
        return httpReq.get();
    }


    public static int getStatusCode(Response response){
        return response.getStatusCode();
    }


}