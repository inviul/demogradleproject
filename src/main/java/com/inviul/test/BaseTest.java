package com.inviul.test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This is the common class which will be shared among all the test cases. It contains generic methods which applies at test level.
 */

public class BaseTest {
    public static Logger logger = getLogData(BaseTest.class.getName());

    public static Logger getLogData(String className){
        DOMConfigurator.configure("log4j.xml");
        return Logger.getLogger(className);
    }

    public static ExtentTest generateTest(ExtentReports report, String testName, String desc){
        ExtentTest test = report.startTest(testName, desc);
        return test;
    }

    public static void reportFail(ExtentTest test, String methodName, String details){
        test.log(LogStatus.FAIL, methodName, details);
    }

    public static void reportPass(ExtentTest test, String methodName, String details){
        test.log(LogStatus.PASS, methodName, details);
    }

    public static void tearDown(ExtentTest test, ExtentReports report){
        report.endTest(test);
        report.flush();
        report.close();
    }

}
